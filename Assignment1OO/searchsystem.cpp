#include "searchsystem.h"
#include <iostream>
#include <string>
#include <sstream>
#include <stack>
#include <queue>
#include <list>
#include <iterator>
#include "bnode.h"
#include "anode.h"


enum dir { left, right, up, down, none };

bool manualset = false;
bool drawsteps = true;


int searchsystem::initialize()
{
	for (unsigned y = 0; y < size; ++y)
	{
		for (unsigned x = 0; x < size; ++x)
		{
			grids.push_back(grid(x, y, '-'));
		}
	}
	goal = { { { { 1, 1 } }, { { 1, 2 } }, { { 1, 3 } } } };
	defaultstart = { { { { 0, 3 } }, { { 1, 3 } }, { { 2, 3 } }, { { 3, 3 } } } };
	/*if (manualset)
		takeparams();
	else
		takeparams(defaultstart);*/
	takeparams(defaultstart);
	draw();
	return true;
}

int searchsystem::takeparams(array<array<int, 2>, 4>& defaultstart)
{
	for (unsigned i = 0; i < 4; ++i)
	{
		abcx[i] = &grids[defaultstart[i][1] * size + defaultstart[i][0]];
		abcx[i]->value = 'A' + i;
	}
	abcx[3]->value = 'X';
	return true;
}


int searchsystem::draw()
{
	for (unsigned y = 0; y < size; ++y)
	{
		for (unsigned x = 0; x < size; ++x)
		{
			printf("%c ", grids[y*size + x].value);
		}
		printf("\n");
	}
	printf("\n");
	return true;
}


int searchsystem::draw(array<array<int, 2>, 4>& curlayout)
{
	unsigned index;
	for (unsigned i = 0; i < size; ++i)
	{
		for (unsigned j = 0; j < size; ++j)
		{
			index = i*size + j;
			if (index == curlayout[0][1] * size + curlayout[0][0])
				printf("A ");
			else if (index == curlayout[1][1] * size + curlayout[1][0])
				printf("B ");
			else if (index == curlayout[2][1] * size + curlayout[2][0])
				printf("C ");
			else if (index == curlayout[3][1] * size + curlayout[3][0])
				printf("X ");
			else
				printf("- ");
		}
		printf("\n");
	}
	printf("\n");
	return true;
}


int searchsystem::dfs(unsigned depth)
{
	stack<node> visitlist;
	vector<node> solution;
	solution.resize(depth);
	vector<vector<node>> solutions;

	node parent(dir::none, 0, defaultstart);
	visitlist.push(parent);
	
	while (visitlist.size() != 0)
	{
		node* parent_ptr = &(visitlist.top());
		//depth check
		if (parent_ptr->depth > depth-1|| !parent_ptr->useful())
		{
			visitlist.pop();
			continue;
		}
		
		for (unsigned i = 0; i < 4; ++i)
		{
			if (parent_ptr->branches[i])
			{
				//explored
				parent_ptr->branches[i] = false;
				//check if this direction is possible
				if (parent_ptr->checkchild(i,size))
				{
					node child(i, parent_ptr->depth+1,parent_ptr->abcx);
					//draw(child.abcx);
					solution[child.depth - 1] = child;
					//if current node match the goal state
					if (child.verify(goal))
					{
						//if match, then remove its father node 
						//since other direction from its father node is
						//meaningless to explore.
						solution.erase(solution.begin() + child.depth,solution.end());
						visitlist.pop();
						solutions.push_back(solution);
						solution.resize(depth);
					}
					else
						//not matching goal state, keep explore.
						visitlist.push(child);
					break;
				}
				
			}
		}
	}
	vector<node>* best;
	vector<node>* worst;
	best = &(solutions.front());
	worst = &(solutions.front());
	for (unsigned i = 0; i < solutions.size(); ++i)
	{
		if (best->size() > solutions[i].size())
			best = &(solutions[i]);
		if (worst->size() < solutions[i].size())
			worst = &(solutions[i]);
	}
	printf("The best solution.\n");
	drawsolution(*best);
	printf("The worst solution.\n");
	drawsolution(*worst);

 	return 1;
}

int searchsystem::bfs(unsigned depth){
	queue<bnode> visitlist;
	
	vector<int> solution;

	bnode parent(dir::none, 0, defaultstart);
	visitlist.push(parent);
	
	while (visitlist.size() != 0)
	{
		bnode* parent_ptr = &(visitlist.front());
		//depth check
		if (parent_ptr->depth > depth - 1)
		{
			return true;
		}
		for (unsigned i = 0; i < 4; ++i)
		{
			if (parent_ptr->branches[i])
			{
				//explored
				parent_ptr->branches[i] = false;
				//check if this direction is possible
				if (parent_ptr->checkchild(i, size))
				{
					bnode child(i, parent_ptr->depth + 1, parent_ptr->abcx);
					//draw(child.abcx);
					child.record = parent_ptr->record;
					child.addrecord(i);
					//if current node match the goal state
					if (child.verify(goal))
					{
						//if match, then remove its father node 
						//since other direction from its father node is
						//meaningless to explore.
						solution = child.record;
						//visitlist.pop();
						//break;
						//Draw the first solution found
						drawsolution(solution);
						return true;
					}
					else
						//not matching goal state, keep explore.
						visitlist.push(child);
				}
			}
		}
		visitlist.pop();
	}
	
	return true;
}

int searchsystem::iddfs(unsigned depth)
{
	stack<bnode> visitlist;

	vector<vector<int>> solutions; 
	

	bnode parent(dir::none, 0, defaultstart);
	visitlist.push(parent);

	for (unsigned iter_depth = 1; iter_depth < depth; ++iter_depth)
	{
		while (visitlist.size() != 0)
		{
			bnode* parent_ptr = &(visitlist.top());
			//depth check
			if (parent_ptr->depth > iter_depth || !parent_ptr->useful())
			{
				visitlist.pop();
				continue;
			}

			for (unsigned i = 0; i < 4; ++i)
			{
				if (parent_ptr->branches[i])
				{
					//explored
					parent_ptr->branches[i] = false;
					//check if this direction is possible
					if (parent_ptr->checkchild(i, size))
					{
						bnode child(i, parent_ptr->depth + 1, parent_ptr->abcx);
						//draw(child.abcx);
						child.record = parent_ptr->record;
						child.addrecord(i);
						//save more time if only check the deepest nodes
						if (child.depth == iter_depth)
						{
							//if current node match the goal state
							if (child.verify(goal))
							{
								//if match, then remove its father node 
								//since other direction from its father node is
								//meaningless to explore.
								visitlist.pop();
								solutions.push_back(child.record);
							}
						}
						
						else
							//not matching goal state, keep explore.
							visitlist.push(child);
						break;
					}

				}
			}
		}
		visitlist.push(parent);
	}

	drawsolution(solutions[0]);

	return true;


}

//This function sort the expanded child nodes into the vector of astar
void sortlist(vector<anode>& nlist, anode& child)
{
	for (unsigned i = 0; i < nlist.size(); ++i)
	{
		//Keep comparing until it reaches the one that is smaller than the child
		if (child.heuristic < nlist[i].heuristic)
			continue;
		else
		{
			nlist.insert(nlist.begin() + i, child);
			return;
		}
	}
	//Loop over the list and no other nodes is smaller.
	//However, the current parent nodes is still in the
	//last one incase other child nodes are not expanded
	nlist.insert(nlist.end()-1, child);
}


int searchsystem::astar()
{
	anode parent(dir::none, 0, defaultstart);
	parent.calc_h(goal);
	//Store all the unexpanded nodes
	vector<anode> nodes;
	
	bool solved = false;
	
	nodes.push_back(parent);

	bnode* parent_ptr;


	while (!solved)
	{
		for (unsigned i = 0; i < 4; ++i)
		{
			//Expand the node at the back of the vector
			if (nodes.back().branches[i])
			{
				if (nodes.back().checkchild(i, size))
				{
					anode child(i, nodes.back().depth + 1, nodes.back().abcx);
					child.record =nodes.back().record;
					child.addrecord(i);
					child.calc_h(goal);
					//draw(child.abcx);
					//Add the child into the vector and sort the list
					sortlist(nodes, child);
					//Find the solution. return the result
					if (child.verify(goal)){
						solved = true;
						//draw the solution
						drawsolution(child.record);
						return true;
					}
				}

			}
		}
		nodes.pop_back();
	}

	return true;
}

void searchsystem::drawsolution(vector<int>& solution)
{
	printf("The length of solution is %d : \n", solution.size());
	vector<node> nodes;
	nodes.reserve(solution.size());
	node parent(dir::none, 0, defaultstart);
	draw(parent.abcx);
	nodes.push_back(parent);
	for (unsigned i = 0; i < solution.size(); ++i)
	{
		node child(solution[i], nodes.back().depth + 1, nodes.back().abcx);
		nodes.push_back(child);
		draw(child.abcx);
	}
}

void searchsystem::drawsolution(vector<node>& solution)
{
	printf("The length of the solution is %d : \n", solution.size());
	for (unsigned i = 0; i < solution.size(); ++i)
	{
		draw(solution[i].abcx);
	}
}
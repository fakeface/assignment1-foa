#pragma once
#include "node.h"
#include <vector>

using namespace std;

class bnode :public node{
public:
	vector<int> record;

	bnode(unsigned dir, unsigned _depth, array<array<int, 2>, 4> child) :node(dir,_depth,child){}
	~bnode(){}

	void addrecord(int i){
		record.push_back(i);
	}

	vector<int>* getsolution()
	{
		return &record;
	}

};
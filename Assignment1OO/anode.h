#pragma once
#include "bnode.h"

using namespace std;

class anode :public bnode
{
public:
	int heuristic;

	anode(unsigned dir, unsigned _depth, array<array<int, 2>, 4> child) :bnode(dir, _depth, child){ heuristic = 0; }
	~anode(){}

	void calc_h(array<array<int,2>,3>& goal){
		int g = record.size() + 1;
		int f = 0;
		for (unsigned i = 0; i < 3; ++i)
		{
			//Manhatten distance
			f += abs(abcx[i][0] - goal[i][0]) + abs(abcx[i][1] - goal[i][1]);
		}
		heuristic = f + g;
	}

	int get_h()
	{
		return heuristic;
	}
};



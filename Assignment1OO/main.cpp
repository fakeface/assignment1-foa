#include "searchsystem.h";

int main(){
	searchsystem s(4);
	s.initialize();
	printf("Depth first search\n");
	s.dfs(20);
	printf("Breadth first search\n");
	s.bfs(15);
	printf("Iterrative deepening search");
	s.iddfs(15);
	printf("A* search");
	s.astar();
}
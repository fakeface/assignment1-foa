#pragma once
#include<array>

using namespace std;

class node
{
private:
public:
	unsigned comingdir;
	unsigned depth;
	unsigned x, y;
	array<array<int, 2>, 4> abcx;
	array<bool, 4> branches;

	node(){}
	node(unsigned dir, unsigned _depth, array<array<int, 2>, 4> child) :comingdir(dir), depth(_depth), abcx(child)
	{
		for (unsigned i = 0; i < 4; ++i)
		{
			branches[i] = true;
		}
		switch (dir)
		{
			//no coming dir(parent)
		default:
			break;
			//left,x minus 1
		case 0:
			branches[1] = false;
			abcx[3][0] += -1;
			checkoverlap(1, 0);
			break;
			//right,x plus 1
		case 1:
			branches[0] = false;
			abcx[3][0] += 1;
			checkoverlap(-1, 0);
			break;
			//up,y minus 1
		case 2:
			branches[3] = false;
			abcx[3][1] += -1;
			checkoverlap(0, 1);
			break;
			//down,y plus 1
		case 3:
			branches[2] = false;
			abcx[3][1] += 1;
			checkoverlap(0, -1);
			break;
		}
		//extract position
		x = abcx[3][0];
		y = abcx[3][1];
	}
	~node(){}

	//return true if any nodes are not explored
	bool useful(){ return branches[0] || branches[1] || branches[2] || branches[3]; }
	//return true if child node can be branched with the index direction
	bool checkchild(unsigned index,unsigned size){
		switch (index)
		{
		default:
			return false;
			break;
			//left
		case 0:
			if (x > 0)
				return true;
			break;
			//right
		case 1:
			if (x < size - 1)
				return true;
			break;
			//up
		case 2:
			if (y > 0)
				return true;
			break;
			//down
		case 3:
			if (y < size - 1)
				return true;
			break;
		}
		//out of boundary
		return false;
	}
	//check overlaping
	void checkoverlap(int _x, int _y)
	{
		for (unsigned i = 0; i < 3; ++i)
		{
			if (abcx[i] == abcx[3])
			{
				abcx[i][0] += _x;
				abcx[i][1] += _y;
				return;
			}
		}
	}
	//verify the result
	bool verify(array<array<int,2>,3>& goal){
		for (unsigned i = 0; i < 3; ++i)
		{
			if (abcx[i] != goal[i])
				return false;
		}
		return true;
	}
};
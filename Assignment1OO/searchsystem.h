#include "grid.h"
#include <vector>
#include <array>
#include "node.h"

using namespace std;


class searchsystem
{
private:
	//this shows the layout of the grids. 
	//should remain unchanged during the search
	vector<grid> grids;
	grid* abcx[4];
	
public:
	unsigned size;
	array<array<int, 2>, 3> goal;
	array<array<int, 2>, 4> defaultstart;

	searchsystem(unsigned int _size) :size(_size){}
	//searchsystem(unsigned int _size, )
	~searchsystem(){}

	int initialize();
	//int bread_first();
	int dfs(unsigned depth);
	int bfs(unsigned depth);
	int iddfs(unsigned depth);
	int astar();
	//int irrative();
	int draw();
	int draw(array<array<int, 2>, 4>& curlayout);
	//int takeparams();
	int takeparams(array<array<int, 2>, 4>& defaultstart);
	void drawsolution(vector<int>& solution);
	void drawsolution(vector<node>& solution);
	//int chkconstraint(unsigned x, unsigned y, int prev_dir);
	//bool validatesolution();
};